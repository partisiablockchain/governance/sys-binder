package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.contract.ContractEvent;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.EventCreatorImpl;
import com.partisiablockchain.contract.EventManagerImpl;
import com.partisiablockchain.contract.InteractionBuilder;
import com.partisiablockchain.contract.sys.ContractEventDeploy;
import com.partisiablockchain.contract.sys.ContractEventUpgrade;
import com.partisiablockchain.contract.sys.Deploy;
import com.partisiablockchain.contract.sys.DeployBuilder;
import com.partisiablockchain.contract.sys.DeployBuilderImpl;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.Governance;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.contract.sys.Upgrade;
import com.partisiablockchain.contract.sys.UpgradeBuilder;
import com.partisiablockchain.contract.sys.UpgradeBuilderImpl;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

final class SysContext implements SysContractContext {

  private final SysBinderContext context;
  private long usedGas;
  private final List<ContractEvent> invocations;
  private ContractEventGroup remoteCalls;
  private byte[] result;

  SysContext(SysBinderContext context) {
    this.context = context;
    this.invocations = new ArrayList<>();
  }

  void deductFee(long cost) {
    if (getContractAddress().getType() != BlockchainAddress.Type.CONTRACT_GOV) {
      this.context.registerCpuFee(cost);
      usedGas += cost;
    }
  }

  @Override
  public BlockchainAddress getContractAddress() {
    return context.getContractAddress();
  }

  @Override
  public SystemEventCreator getInvocationCreator() {
    return new SystemEventCreatorImpl(invocations, this::deductFee);
  }

  @Override
  public SystemEventManager getRemoteCallsCreator() {
    if (remoteCalls == null) {
      remoteCalls = new ContractEventGroup();
    }
    return new SystemEventManagerImpl(remoteCalls, this::deductFee);
  }

  @Override
  public void setResult(DataStreamSerializable result) {
    this.result = SafeDataOutputStream.serialize(result);
  }

  @Override
  public long getBlockTime() {
    return context.getBlockTime();
  }

  @Override
  public long getBlockProductionTime() {
    return context.getBlockProductionTime();
  }

  @Override
  public BlockchainAddress getFrom() {
    return context.getFrom();
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return context.getCurrentTransactionHash();
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return context.getOriginalTransactionHash();
  }

  @Override
  public StateSerializable getGlobalAccountPluginState() {
    return context.getGlobalAccountPluginState();
  }

  @Override
  public PluginInteractionCreator getAccountPluginInteractions() {
    return context.getAccountPluginInteractions();
  }

  @Override
  public StateSerializable getGlobalConsensusPluginState() {
    return context.getGlobalConsensusPluginState();
  }

  @Override
  public PluginInteractionCreator getConsensusPluginInteractions() {
    return context.getConsensusPluginInteractions();
  }

  @Override
  public StateSerializable getGlobalSharedObjectStorePluginState() {
    return context.getGlobalSharedObjectStorePluginState();
  }

  @Override
  public Governance getGovernance() {
    return context.getGovernance();
  }

  @Override
  public String getFeature(String key) {
    return context.getFeature(key);
  }

  @Override
  public void registerDeductedByocFees(
      Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes) {
    context.registerDeductedByocFees(amount, symbol, nodes);
  }

  @Override
  public void payServiceFees(long gas, BlockchainAddress target) {
    long remainingGas = remainingGas();
    if (gas > remainingGas) {
      throw new IllegalStateException(
          "Unable to pay service fee from transaction. Needed "
              + gas
              + ", but only "
              + remainingGas
              + " available.");
    }
    usedGas += gas;
    context.payServiceFees(gas, target);
  }

  @Override
  public void payInfrastructureFees(long gas, BlockchainAddress target) {
    context.payInfrastructureFees(gas, target);
  }

  void distributeGas() {
    long remainingGas = remainingGas();
    DistributionNeed distributionNeed = new DistributionNeed(remainingGas);
    findNeed(distributionNeed, remoteCalls);
    findNeed(distributionNeed, invocations);

    if (distributionNeed.remainingGas < 0) {
      throw new RuntimeException(
          "Cannot allocate gas for events: " + distributionNeed.remainingGas);
    }

    if (distributionNeed.count > 0) {
      long allowedCost = distributionNeed.remainingGas / distributionNeed.count;
      distribute(allowedCost, remoteCalls);
      distribute(allowedCost, invocations);
    }
  }

  private long remainingGas() {
    return context.availableGas() - usedGas;
  }

  public void distribute(long allowedCost, ContractEventGroup eventGroup) {
    if (eventGroup != null) {
      if (eventGroup.callbackCost == null) {
        eventGroup.callbackCost = allowedCost;
      }
      distribute(allowedCost, eventGroup.contractEvents);
    }
  }

  public void distribute(long allowedCost, List<ContractEvent> eventList) {
    for (ContractEvent contractEvent : eventList) {
      if (contractEvent instanceof ContractEventInteraction event
          && !event.costFromContract
          && event.allocatedCost == null) {
        event.allocatedCost = allowedCost;
      }
      if (contractEvent instanceof ContractEventDeploy event
          && !event.costFromContract
          && event.allocatedCost == null) {
        event.allocatedCost = allowedCost;
      }
      if (contractEvent instanceof ContractEventUpgrade event && event.allocatedCost == null) {
        event.allocatedCost = allowedCost;
      }
    }
  }

  void findNeed(DistributionNeed distributionNeed, ContractEventGroup eventGroup) {
    if (eventGroup != null) {
      if (eventGroup.callbackCost != null) {
        if (!eventGroup.callbackCostFromContract) {
          distributionNeed.remainingGas -= eventGroup.callbackCost;
        }
      } else {
        distributionNeed.count++;
      }
      findNeed(distributionNeed, eventGroup.contractEvents);
    }
  }

  void findNeed(DistributionNeed distributionNeed, List<ContractEvent> eventList) {
    for (ContractEvent contractEvent : eventList) {
      if (contractEvent instanceof ContractEventInteraction event) {
        if (!event.costFromContract) {
          if (event.allocatedCost != null) {
            distributionNeed.remainingGas -= event.allocatedCost;
          } else {
            distributionNeed.count++;
          }
        }
      } else if (contractEvent instanceof ContractEventDeploy deploy && !deploy.costFromContract) {
        if (deploy.allocatedCost != null) {
          distributionNeed.remainingGas -= deploy.allocatedCost;
        } else {
          distributionNeed.count++;
        }
      } else if (contractEvent instanceof ContractEventUpgrade upgrade) {
        if (upgrade.allocatedCost != null) {
          distributionNeed.remainingGas -= upgrade.allocatedCost;
        } else {
          distributionNeed.count++;
        }
      }
    }
  }

  CallResult getCallResult() {
    if (result != null && remoteCalls != null) {
      throw new IllegalStateException("Impossible to have result as well as callback");
    } else if (result != null) {
      return new CallReturnValue(result);
    } else if (remoteCalls != null) {
      List<BinderEvent> binderInteractions =
          remoteCalls.contractEvents.stream().map(this::convert).toList();
      return new BinderEventGroup<>(
          remoteCalls.callbackRpc,
          remoteCalls.callbackCost,
          remoteCalls.callbackCostFromContract,
          binderInteractions);
    } else {
      return null;
    }
  }

  List<BinderEvent> getInteractions() {
    return invocations.stream().map(this::convert).toList();
  }

  private BinderEvent convert(ContractEvent event) {
    if (event instanceof ContractEventInteraction interaction) {
      return new BinderInteraction(
          interaction.contract,
          SafeDataOutputStream.serialize(interaction.rpc),
          interaction.originalSender,
          interaction.allocatedCost,
          interaction.costFromContract);
    } else if (event instanceof ContractEventDeploy deployEvent) {
      Deploy deploy = deployEvent.deploy;
      return new BinderDeploy(
          deploy.contract,
          deploy.binderJar,
          deploy.contractJar,
          deploy.abi,
          SafeDataOutputStream.serialize(deploy.rpc),
          deployEvent.originalSender,
          deployEvent.allocatedCost);
    } else if (event instanceof ContractEventUpgrade upgradeEvent) {
      Upgrade upgrade = upgradeEvent.upgrade;
      return new BinderUpgrade(
          upgrade.contract,
          upgrade.newBinderJar,
          upgrade.newContractJar,
          upgrade.newAbi,
          SafeDataOutputStream.serialize(upgrade.rpc),
          upgradeEvent.originalSender,
          upgradeEvent.allocatedCost);
    } else /* System event */ {
      return ((ContractSystemEvent) event).event;
    }
  }

  abstract static class SystemEventCreatorAbstract implements SystemEventCreator {

    private final List<ContractEvent> invocations;
    private final EventCreatorImpl eventCreator;
    private final Consumer<Long> feeLogger;

    public SystemEventCreatorAbstract(List<ContractEvent> invocations, Consumer<Long> feeLogger) {
      this.invocations = invocations;
      this.eventCreator = new EventCreatorImpl(invocations::add);
      this.feeLogger = feeLogger;
    }

    private void addSystemEvent(SystemInteraction e) {
      feeLogger.accept(SystemContractBinder.COST_OF_INTERACTION);
      invocations.add(new ContractSystemEvent(e));
    }

    @Override
    public void createAccount(BlockchainAddress address) {
      SystemInteraction e = new SystemInteraction.CreateAccount(address);
      addSystemEvent(e);
    }

    @Override
    public void createShard(String shardId) {
      addSystemEvent(new SystemInteraction.CreateShard(shardId));
    }

    @Override
    public void removeShard(String shardId) {
      addSystemEvent(new SystemInteraction.RemoveShard(shardId));
    }

    @Override
    public void updateLocalAccountPluginState(LocalPluginStateUpdate update) {
      addSystemEvent(new SystemInteraction.UpdateLocalAccountPluginState(update));
    }

    @Override
    public void updateContextFreeAccountPluginState(String shardId, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateContextFreeAccountPluginState(shardId, rpc));
    }

    @Override
    public void updateGlobalAccountPluginState(GlobalPluginStateUpdate update) {
      addSystemEvent(new SystemInteraction.UpdateGlobalAccountPluginState(update));
    }

    @Override
    public void updateAccountPlugin(byte[] pluginJar, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateAccountPlugin(pluginJar, rpc));
    }

    @Override
    public void updateLocalConsensusPluginState(LocalPluginStateUpdate update) {
      addSystemEvent(new SystemInteraction.UpdateLocalConsensusPluginState(update));
    }

    @Override
    public void updateGlobalConsensusPluginState(GlobalPluginStateUpdate update) {
      addSystemEvent(new SystemInteraction.UpdateGlobalConsensusPluginState(update));
    }

    @Override
    public void updateConsensusPlugin(byte[] pluginJar, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateConsensusPlugin(pluginJar, rpc));
    }

    @Override
    public void updateRoutingPlugin(byte[] pluginJar, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateRoutingPlugin(pluginJar, rpc));
    }

    @Override
    public void updateGlobalSharedObjectStorePluginState(
        GlobalPluginStateUpdate globalPluginStateUpdate) {
      addSystemEvent(
          new SystemInteraction.UpdateGlobalSharedObjectStorePluginState(globalPluginStateUpdate));
    }

    @Override
    public void updateSharedObjectStorePlugin(byte[] pluginJar, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateSharedObjectStorePlugin(pluginJar, rpc));
    }

    @Override
    public DeployBuilder deployContract(BlockchainAddress contract) {
      return new DeployBuilderImpl(invocations::add, contract);
    }

    @Override
    public UpgradeBuilder upgradeContract(BlockchainAddress contract) {
      return new UpgradeBuilderImpl(invocations::add, contract);
    }

    @Override
    public void removeContract(BlockchainAddress address) {
      addSystemEvent(new SystemInteraction.RemoveContract(address));
    }

    @Override
    public void exists(BlockchainAddress address) {
      addSystemEvent(new SystemInteraction.Exists(address));
    }

    @Override
    public void setFeature(String key, String value) {
      addSystemEvent(new SystemInteraction.SetFeature(key, value));
    }

    @Override
    public void upgradeSystemContract(
        byte[] contractJar, byte[] binderJar, byte[] rpc, BlockchainAddress contractAddress) {
      upgradeSystemContract(contractJar, binderJar, new byte[0], rpc, contractAddress);
    }

    @Override
    public void upgradeSystemContract(
        byte[] contractJar,
        byte[] binderJar,
        byte[] abi,
        byte[] rpc,
        BlockchainAddress contractAddress) {
      addSystemEvent(
          new SystemInteraction.UpgradeSystemContract(
              contractJar, binderJar, rpc, abi, contractAddress));
    }

    @Override
    public InteractionBuilder invoke(BlockchainAddress contract) {
      return eventCreator.invoke(contract);
    }
  }

  static final class SystemEventCreatorImpl extends SystemEventCreatorAbstract {

    public SystemEventCreatorImpl(List<ContractEvent> invocations, Consumer<Long> feeLogger) {
      super(invocations, feeLogger);
    }
  }

  static final class SystemEventManagerImpl extends SystemEventCreatorAbstract
      implements SystemEventManager {

    private final EventManagerImpl eventManager;

    public SystemEventManagerImpl(ContractEventGroup eventGroup, Consumer<Long> feeLogger) {
      super(eventGroup.contractEvents, feeLogger);
      this.eventManager = new EventManagerImpl(eventGroup);
    }

    @Override
    public void registerCallback(DataStreamSerializable rpc, long allocatedCost) {
      eventManager.registerCallback(rpc, allocatedCost);
    }

    @Override
    public void registerCallbackWithCostFromRemaining(DataStreamSerializable rpc) {
      eventManager.registerCallbackWithCostFromRemaining(rpc);
    }

    @Override
    public void registerCallbackWithCostFromContract(
        DataStreamSerializable rpc, long allocatedCost) {
      eventManager.registerCallbackWithCostFromContract(rpc, allocatedCost);
    }
  }

  private static final class DistributionNeed {

    int count = 0;
    long remainingGas;

    public DistributionNeed(long remainingGas) {
      this.remainingGas = remainingGas;
    }
  }

  private static final class ContractSystemEvent implements ContractEvent {

    private final SystemInteraction event;

    private ContractSystemEvent(SystemInteraction event) {
      this.event = event;
    }
  }
}
