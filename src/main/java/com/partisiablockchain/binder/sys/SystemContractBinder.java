package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Default binder contract for binding system contracts with the blockchain.
 *
 * @param <OpenT> the state type of the contract
 */
public final class SystemContractBinder<OpenT extends StateSerializable>
    implements SysBinderContract<OpenT> {

  /** Static cost of system interaction. */
  public static final long COST_OF_INTERACTION = 2_500;

  private final SysContract<OpenT> contract;

  /**
   * Default constructor.
   *
   * @param contract system contract open state
   */
  public SystemContractBinder(SysContract<OpenT> contract) {
    this.contract = contract;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Class<OpenT> getStateClass() {
    Type genericSuperclass = contract.getClass().getGenericSuperclass();
    Type[] typeArguments = ((ParameterizedType) genericSuperclass).getActualTypeArguments();
    return (Class<OpenT>) typeArguments[0];
  }

  @Override
  public OpenT upgrade(StateAccessor oldState, byte[] rpc) {
    OpenT openT =
        SafeDataInputStream.readFully(rpc, stream -> contract.onUpgrade(oldState, stream));
    return openT;
  }

  private BinderResult<OpenT, BinderEvent> execute(
      SysBinderContext context,
      byte[] rpc,
      BiFunction<SysContractContext, SafeDataInputStream, OpenT> function) {
    SysContext sysContext = registerCpuAndCreateContext(context);
    OpenT openT = SafeDataInputStream.readFully(rpc, s -> function.apply(sysContext, s));

    sysContext.distributeGas();
    return new SysBinderResultImpl(openT, sysContext.getInteractions(), sysContext.getCallResult());
  }

  @Override
  public BinderResult<OpenT, BinderEvent> create(SysBinderContext context, byte[] rpc) {
    return execute(context, rpc, contract::onCreate);
  }

  @Override
  public BinderResult<OpenT, BinderEvent> invoke(
      SysBinderContext context, OpenT state, byte[] rpc) {
    return execute(
        context, rpc, (pubContext, stream) -> contract.onInvoke(pubContext, state, stream));
  }

  @Override
  public BinderResult<OpenT, BinderEvent> callback(
      SysBinderContext context, OpenT state, CallbackContext callbackContext, byte[] rpc) {
    return execute(
        context,
        rpc,
        (pubContext, stream) -> contract.onCallback(pubContext, state, callbackContext, stream));
  }

  private SysContext registerCpuAndCreateContext(SysBinderContext context) {
    SysContext sysContext = new SysContext(context);
    sysContext.deductFee(COST_OF_INTERACTION);
    return sysContext;
  }

  private final class SysBinderResultImpl implements BinderResult<OpenT, BinderEvent> {

    private final OpenT openT;
    private final List<BinderEvent> invocations;
    private final CallResult callResult;

    public SysBinderResultImpl(OpenT openT, List<BinderEvent> invocations, CallResult callResult) {
      this.invocations = invocations;
      this.callResult = callResult;
      this.openT = openT;
    }

    @Override
    public CallResult getCallResult() {
      return callResult;
    }

    @Override
    public List<BinderEvent> getInvocations() {
      return invocations;
    }

    @Override
    public OpenT getState() {
      return openT;
    }
  }
}
