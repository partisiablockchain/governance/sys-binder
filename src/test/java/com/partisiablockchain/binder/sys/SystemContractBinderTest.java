package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.sys.Governance;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class SystemContractBinderTest {

  private final BlockchainAddress owner =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainAddress from =
      BlockchainAddress.fromString("000070000000000000000000000000000000000001");
  private BlockchainAddress contractAddress =
      BlockchainAddress.fromString("010000000000000000000000000000000000000001");
  private final Governance governance = Mockito.mock(Governance.class);
  private final PluginInteractionCreator accountInteractions =
      Mockito.mock(PluginInteractionCreator.class);
  private final PluginInteractionCreator consensusInteractions =
      Mockito.mock(PluginInteractionCreator.class);

  private final StateSerializable globalAccountPluginState = Mockito.mock(StateSerializable.class);
  private final StateSerializable globalConsensusPluginState =
      Mockito.mock(StateSerializable.class);

  private final StateSerializable globalGovernanceStoragePluginState =
      Mockito.mock(StateSerializable.class);
  private static final long productionTime = 11L;
  private static final long blockTime = 77L;
  private static final Hash currentTransactionHash = Hash.create(stream -> stream.writeInt(42));
  private static final Hash originalTransactionHash = Hash.create(stream -> stream.writeInt(17));
  private static final long AVAILABLE_GAS = 12345L;

  private final TestSysContract contract = new TestSysContract();
  private final SystemContractBinder<StateString> binder = new SystemContractBinder<>(contract);
  private final SysBinderContext sysBinderContext = Mockito.mock(SysBinderContext.class);

  /** Setup mocks and contract. */
  @BeforeEach
  public void setUp() {
    Mockito.when(sysBinderContext.getContractAddress()).thenReturn(contractAddress);
    Mockito.when(sysBinderContext.getBlockProductionTime()).thenReturn(productionTime);
    Mockito.when(sysBinderContext.getBlockTime()).thenReturn(blockTime);
    Mockito.when(sysBinderContext.getFrom()).thenReturn(from);
    Mockito.when(sysBinderContext.getCurrentTransactionHash()).thenReturn(currentTransactionHash);
    Mockito.when(sysBinderContext.getOriginalTransactionHash()).thenReturn(originalTransactionHash);
    Mockito.when(sysBinderContext.getGovernance()).thenReturn(governance);
    Mockito.when(sysBinderContext.getAccountPluginInteractions()).thenReturn(accountInteractions);
    Mockito.when(sysBinderContext.getGlobalAccountPluginState())
        .thenReturn(globalAccountPluginState);
    Mockito.when(sysBinderContext.getConsensusPluginInteractions())
        .thenReturn(consensusInteractions);
    Mockito.when(sysBinderContext.getGlobalConsensusPluginState())
        .thenReturn(globalConsensusPluginState);
    Mockito.when(sysBinderContext.getGlobalSharedObjectStorePluginState())
        .thenReturn(globalGovernanceStoragePluginState);
    Mockito.when(sysBinderContext.availableGas()).thenReturn(AVAILABLE_GAS);
    Mockito.when(sysBinderContext.getFeature("Feature")).thenReturn("Value");
  }

  @Test
  public void getStateClass() {
    Assertions.assertThat(binder.getStateClass()).isAssignableFrom(StateString.class);
  }

  @Test
  public void dummyCallForEvents() {
    SystemContractBinder<StateString> contractBinder =
        new SystemContractBinder<>(new TestSysContractAbstract() {});
    BinderResult<StateString, BinderEvent> invoke =
        contractBinder.invoke(sysBinderContext, null, new byte[] {33});
    StateString state = invoke.getState();
    Assertions.assertThat(state).isNotNull();
    Assertions.assertThat(state.value()).isEqualTo("invoke");
    Assertions.assertThat(invoke.getInvocations()).hasSize(1);
    Assertions.assertThat(invoke.getCallResult()).isNull();
  }

  @Test
  public void dummyCallResult() {
    SystemContractBinder<StateString> contractBinder =
        new SystemContractBinder<>(new TestSysContractAbstract() {});
    BinderResult<StateString, BinderEvent> invoke =
        contractBinder.invoke(sysBinderContext, null, new byte[] {77});
    Assertions.assertThat(invoke.getCallResult()).isNotNull().isInstanceOf(CallReturnValue.class);
    CallReturnValue result = (CallReturnValue) invoke.getCallResult();
    Assertions.assertThat(result.getResult()).isEqualTo(new byte[] {0, 0, 0, 0, 0, 0, 4, -46});
  }

  @Test
  public void create() {
    StateString result =
        getState(
            binder.create(sysBinderContext, new byte[] {33}),
            BlockchainAddress.Type.CONTRACT_SYSTEM);

    verify(new int[] {1, 0, 0, 0, 0});
    Assertions.assertThat(contract.latestState).isNull();
    Assertions.assertThat(result.value()).isEqualTo("create");
  }

  @Test
  public void invoke() {
    StateString state = new StateString();
    StateString result =
        getState(
            binder.invoke(sysBinderContext, state, new byte[] {33}),
            BlockchainAddress.Type.CONTRACT_SYSTEM);

    verify(new int[] {0, 1, 0, 0, 0});
    Assertions.assertThat(contract.latestState).isEqualTo(state);
    Assertions.assertThat(result.value()).isEqualTo("invoke");
  }

  @Test
  public void noCpuForGovernance() {
    contractAddress = BlockchainAddress.fromString("040000000000000000000000000000000000000001");
    Mockito.when(sysBinderContext.getContractAddress()).thenReturn(contractAddress);
    StateString state = new StateString();

    StateString result =
        getState(
            binder.invoke(sysBinderContext, state, new byte[] {33}),
            BlockchainAddress.Type.CONTRACT_GOV);
    Assertions.assertThat(result.value()).isEqualTo("invoke");
    Assertions.assertThat(contract.latestContext).isNotNull();

    Mockito.verify(sysBinderContext, Mockito.never()).registerCpuFee(Mockito.anyLong());
  }

  @Test
  public void callback() {
    StateString state = new StateString();
    StateString result =
        getState(
            binder.callback(
                sysBinderContext,
                state,
                CallbackContext.create(FixedList.create()),
                new byte[] {33}),
            BlockchainAddress.Type.CONTRACT_SYSTEM);

    verify(new int[] {0, 0, 1, 0, 0});
    Assertions.assertThat(contract.latestState).isEqualTo(state);
    Assertions.assertThat(result.value()).isEqualTo("callback");
  }

  private StateString getState(
      BinderResult<StateString, BinderEvent> result, BlockchainAddress.Type type) {
    Assertions.assertThat(result.getCallResult()).isNull();
    Assertions.assertThat(result.getInvocations()).hasSize(1);
    BinderEvent systemBinderEvent = result.getInvocations().get(0);
    Assertions.assertThat(systemBinderEvent).isInstanceOf(BinderInteraction.class);
    BinderInteraction event = (BinderInteraction) systemBinderEvent;
    Assertions.assertThat(event.contract).isEqualTo(contractAddress);
    if (type.equals(BlockchainAddress.Type.CONTRACT_GOV)) {
      Assertions.assertThat(event.effectiveCost).isEqualTo(AVAILABLE_GAS);
    } else {
      Assertions.assertThat(event.effectiveCost)
          .isEqualTo(AVAILABLE_GAS - SystemContractBinder.COST_OF_INTERACTION);
    }
    Assertions.assertThat(event.originalSender).isTrue();
    return result.getState();
  }

  @Test
  public void upgrade() {
    StateString state = new StateString();
    StateAccessor stateAccessor = StateAccessor.create(state);
    StateString upgrade = binder.upgrade(stateAccessor, new byte[] {33});

    Assertions.assertThat(upgrade.value()).isEqualTo("upgrade");
    Assertions.assertThat(contract.invokes).isEqualTo(new int[] {0, 0, 0, 0, 1});
    Assertions.assertThat(contract.latestStateAccessor).isEqualTo(stateAccessor);
    Assertions.assertThat(contract.latestRpc).isEqualTo((byte) 33);
  }

  private void verify(int[] invokes) {
    Assertions.assertThat(contract.invokes).isEqualTo(invokes);

    byte value = contract.latestRpc;
    Assertions.assertThat(value).isEqualTo((byte) 33);

    SysContractContext contractContext = contract.latestContext;
    Assertions.assertThat(contractContext).isNotNull();

    Assertions.assertThat(contractContext.getContractAddress()).isEqualTo(contractAddress);
    Assertions.assertThat(contractContext.getInvocationCreator()).isNotNull();
    SystemEventManager eventManager = contractContext.getRemoteCallsCreator();
    Assertions.assertThat(eventManager).isNotNull();
    Assertions.assertThat(contractContext.getBlockTime()).isEqualTo(blockTime);
    Assertions.assertThat(contractContext.getBlockProductionTime()).isEqualTo(productionTime);
    Assertions.assertThat(contractContext.getFrom()).isEqualTo(from);
    Assertions.assertThat(contractContext.getGovernance()).isEqualTo(governance);
    Assertions.assertThat(contractContext.getCurrentTransactionHash())
        .isEqualTo(currentTransactionHash);
    Assertions.assertThat(contractContext.getOriginalTransactionHash())
        .isEqualTo(originalTransactionHash);
    Assertions.assertThat(contractContext.getAccountPluginInteractions())
        .isEqualTo(accountInteractions);
    Assertions.assertThat(contractContext.getGlobalAccountPluginState())
        .isEqualTo(globalAccountPluginState);
    Assertions.assertThat(contractContext.getConsensusPluginInteractions())
        .isEqualTo(consensusInteractions);
    Assertions.assertThat(contractContext.getGlobalConsensusPluginState())
        .isEqualTo(globalConsensusPluginState);
    Assertions.assertThat(contractContext.getGlobalSharedObjectStorePluginState())
        .isEqualTo(globalGovernanceStoragePluginState);

    Assertions.assertThat(contractContext.getFeature("Feature")).isEqualTo("Value");

    Mockito.verify(sysBinderContext).registerCpuFee(SystemContractBinder.COST_OF_INTERACTION);

    FixedList<BlockchainAddress> targets = FixedList.create(List.of(owner));
    FixedList<Integer> weights = FixedList.create(List.of(1));
    long gas = 42L;
    contractContext.payServiceFees(gas, targets, weights);
    Mockito.verify(sysBinderContext).payServiceFees(gas, owner);
    contractContext.payInfrastructureFees(gas, targets, weights);
    Mockito.verify(sysBinderContext).payInfrastructureFees(gas, owner);

    contractContext.registerDeductedByocFees(Unsigned256.create(42), "abc", FixedList.create());
    Mockito.verify(sysBinderContext)
        .registerDeductedByocFees(Unsigned256.create(42), "abc", FixedList.create());
  }

  private abstract static class TestSysContractAbstract extends SysContract<StateString> {

    int[] invokes = new int[] {0, 0, 0, 0, 0};

    private byte latestRpc;

    @Override
    public StateString onCreate(SysContractContext context, SafeDataInputStream rpc) {
      return new StateString("create");
    }

    @Override
    public StateString onInvoke(
        SysContractContext context, StateString state, SafeDataInputStream rpc) {
      latestRpc = rpc.readSignedByte();
      Assertions.assertThatThrownBy(rpc::readSignedByte)
          .isInstanceOf(RuntimeException.class)
          .hasMessageContaining("Unable to read");
      invokes[1]++;

      if (latestRpc == 77) {
        context.setResult(s -> s.writeLong(1234));
      }
      context
          .getInvocationCreator()
          .invoke(context.getContractAddress())
          .withPayload(EventCreator.EMPTY_RPC)
          .allocateRemainingCost()
          .send();
      return new StateString("invoke");
    }
  }

  private static final class TestSysContract extends SysContract<StateString> {

    int[] invokes = new int[] {0, 0, 0, 0, 0};

    private SysContractContext latestContext;
    private StateString latestState;
    private byte latestRpc;
    private StateAccessor latestStateAccessor;

    @Override
    public StateString onCreate(SysContractContext context, SafeDataInputStream rpc) {
      latestContext = context;
      latestRpc = rpc.readSignedByte();
      Assertions.assertThatThrownBy(rpc::readSignedByte)
          .isInstanceOf(RuntimeException.class)
          .hasMessageContaining("Unable to read");
      invokes[0]++;

      context
          .getInvocationCreator()
          .invoke(context.getContractAddress())
          .withPayload(EventCreator.EMPTY_RPC)
          .allocateRemainingCost()
          .send();
      return new StateString("create");
    }

    @Override
    public StateString onInvoke(
        SysContractContext context, StateString state, SafeDataInputStream rpc) {
      latestContext = context;
      latestState = state;
      latestRpc = rpc.readSignedByte();
      Assertions.assertThatThrownBy(rpc::readSignedByte)
          .isInstanceOf(RuntimeException.class)
          .hasMessageContaining("Unable to read");
      invokes[1]++;

      if (latestRpc == 77) {
        context.setResult(s -> s.writeLong(1234));
      }
      context
          .getInvocationCreator()
          .invoke(context.getContractAddress())
          .withPayload(EventCreator.EMPTY_RPC)
          .allocateRemainingCost()
          .send();
      return new StateString("invoke");
    }

    @Override
    public StateString onCallback(
        SysContractContext context,
        StateString state,
        CallbackContext callbackContext,
        SafeDataInputStream rpc) {
      latestContext = context;
      latestState = state;
      latestRpc = rpc.readSignedByte();
      Assertions.assertThatThrownBy(rpc::readSignedByte)
          .isInstanceOf(RuntimeException.class)
          .hasMessageContaining("Unable to read");
      invokes[2]++;
      context
          .getInvocationCreator()
          .invoke(context.getContractAddress())
          .withPayload(EventCreator.EMPTY_RPC)
          .allocateRemainingCost()
          .send();
      return new StateString("callback");
    }

    @Override
    public StateString onUpgrade(StateAccessor oldState, SafeDataInputStream rpc) {
      latestStateAccessor = oldState;
      latestRpc = rpc.readSignedByte();
      Assertions.assertThatThrownBy(rpc::readSignedByte)
          .isInstanceOf(RuntimeException.class)
          .hasMessageContaining("Unable to read");
      invokes[4]++;
      return new StateString("upgrade");
    }
  }
}
