package com.partisiablockchain.binder.sys;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.sys.Governance;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SysContextTest {

  private static final long AVAILABLE_GAS = 12345 + 20;
  private SystemEventManager systemEventManager;
  private BlockchainAddress address;
  private SysContext sysContext;
  private Hash hash;
  private long registeredFee;

  /** Test. */
  @BeforeEach
  public void setUp() {
    hash = Hash.create(FunctionUtility.noOpConsumer());
    sysContext =
        new SysContext(
            new SysBinderContext() {

              @Override
              public void payServiceFees(long gas, BlockchainAddress target) {}

              @Override
              public void payServiceFees(
                  long gas, FixedList<BlockchainAddress> nodes, FixedList<Integer> weights) {
                SysBinderContext.super.payServiceFees(gas, nodes, weights);
              }

              @Override
              public void payInfrastructureFees(long gas, BlockchainAddress target) {}

              @Override
              public void payInfrastructureFees(
                  long gas, FixedList<BlockchainAddress> nodes, FixedList<Integer> weights) {
                SysBinderContext.super.payInfrastructureFees(gas, nodes, weights);
              }

              @Override
              public StateSerializable getGlobalAccountPluginState() {
                return null;
              }

              @Override
              public PluginInteractionCreator getAccountPluginInteractions() {
                return null;
              }

              @Override
              public StateSerializable getGlobalConsensusPluginState() {
                return null;
              }

              @Override
              public PluginInteractionCreator getConsensusPluginInteractions() {
                return null;
              }

              @Override
              public StateSerializable getGlobalSharedObjectStorePluginState() {
                return null;
              }

              @Override
              public Governance getGovernance() {
                return null;
              }

              @Override
              public String getFeature(String key) {
                return null;
              }

              @Override
              public void registerDeductedByocFees(
                  Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes) {}

              @Override
              public long availableGas() {
                return AVAILABLE_GAS;
              }

              @Override
              public void registerCpuFee(long l) {
                registeredFee += l;
              }

              @Override
              public BlockchainAddress getContractAddress() {
                return address;
              }

              @Override
              public long getBlockTime() {
                return 1234;
              }

              @Override
              public long getBlockProductionTime() {
                return 1234;
              }

              @Override
              public BlockchainAddress getFrom() {
                return address;
              }

              @Override
              public Hash getCurrentTransactionHash() {
                return hash;
              }

              @Override
              public Hash getOriginalTransactionHash() {
                return hash;
              }
            });
    address = new KeyPair().getPublic().createAddress();
  }

  @Test
  public void gasSystem() {
    sysContext.deductFee(123);
    sysContext.distributeGas();
    Assertions.assertThat(registeredFee).isEqualTo(123);
    address = BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_GOV, hash);
    sysContext.deductFee(4444);
    sysContext.distributeGas();
    Assertions.assertThat(registeredFee).isEqualTo(123);
  }

  @Test
  public void createAccount() {
    verify(() -> systemEventManager.createAccount(address));
  }

  @Test
  public void createShard() {
    verify(() -> systemEventManager.createShard("shardId"));
  }

  @Test
  public void removeShard() {
    verify(() -> systemEventManager.removeShard("shardId"));
  }

  @Test
  public void updateLocalAccountPluginState() {
    verify(() -> systemEventManager.updateLocalAccountPluginState(null));
  }

  @Test
  public void updateContextFreeAccountPluginState() {
    verify(() -> systemEventManager.updateContextFreeAccountPluginState("shardId", null));
  }

  @Test
  public void updateGlobalAccountPluginState() {
    verify(() -> systemEventManager.updateGlobalAccountPluginState(null));
  }

  @Test
  public void updateAccountPlugin() {
    verify(() -> systemEventManager.updateAccountPlugin(null, null));
  }

  @Test
  public void updateLocalConsensusPluginState() {
    verify(() -> systemEventManager.updateLocalConsensusPluginState(null));
  }

  @Test
  public void updateGlobalConsensusPluginState() {
    verify(() -> systemEventManager.updateGlobalConsensusPluginState(null));
  }

  @Test
  public void updateConsensusPlugin() {
    verify(() -> systemEventManager.updateConsensusPlugin(null, null));
  }

  @Test
  public void updateRoutingPlugin() {
    verify(() -> systemEventManager.updateRoutingPlugin(null, null));
  }

  @Test
  public void updateGlobalGovernanceStoragePluginState() {
    verify(() -> systemEventManager.updateGlobalSharedObjectStorePluginState(null));
  }

  @Test
  public void updateGovernanceStoragePlugin() {
    verify(() -> systemEventManager.updateSharedObjectStorePlugin(null, null));
  }

  @Test
  public void removeContract() {
    verify(() -> systemEventManager.removeContract(address));
  }

  @Test
  public void exists() {
    verify(() -> systemEventManager.exists(address));
  }

  @Test
  public void setFeature() {
    verify(() -> systemEventManager.setFeature("key", null));
  }

  @Test
  public void upgradeSystemContract() {
    verify(() -> systemEventManager.upgradeSystemContract(null, null, null, null));
  }

  @Test
  public void registerCallback() {
    systemEventManager = sysContext.getRemoteCallsCreator();
    systemEventManager.registerCallback(EventCreator.EMPTY_RPC, 123);
    systemEventManager.registerCallback(EventCreator.EMPTY_RPC, 0);
    Assertions.assertThatThrownBy(
            () -> systemEventManager.registerCallback(EventCreator.EMPTY_RPC, -1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cost must be 0 or larger: -1");
  }

  private void verify(Runnable test) {
    systemEventManager = sysContext.getRemoteCallsCreator();
    test.run();
    sysContext.getInvocationCreator().invoke(address).withPayload(EventCreator.EMPTY_RPC).send();
    SystemEventManager remoteCallsCreator = sysContext.getRemoteCallsCreator();
    remoteCallsCreator.registerCallback(EventCreator.EMPTY_RPC, 432);
    sysContext.distributeGas();

    CallResult remoteCalls = sysContext.getCallResult();
    Assertions.assertThat(remoteCalls).isInstanceOf(BinderEventGroup.class);
    BinderEventGroup<?> convert = (BinderEventGroup<?>) remoteCalls;
    Assertions.assertThat(convert.getCallbackRpc()).hasSize(0);
    Assertions.assertThat(convert.getEvents()).hasSize(1);
    BinderEvent actual = convert.getEvents().get(0);
    Assertions.assertThat(actual).isInstanceOf(SystemInteraction.class);

    List<BinderEvent> interactions = sysContext.getInteractions();
    Assertions.assertThat(interactions).hasSize(1);
    Assertions.assertThat(((BinderInteraction) interactions.get(0)).effectiveCost).isEqualTo(9433L);
  }

  @Test
  public void eventsWithCosts() {
    testEvents(123L, 1474L, false);
  }

  @Test
  public void eventsWithCostsFromContract() {
    testEvents(123L, 1499L, true);
  }

  @Test
  public void eventsWithoutCosts() {
    testEvents(null, 1249L, false);
  }

  @SuppressWarnings("deprecation")
  private void testEvents(Long cost, long computedSplit, boolean fromContract) {
    SystemEventManager eventManager = sysContext.getRemoteCallsCreator();
    eventManager.invoke(address).allocateRemainingCost().withPayload(EventCreator.EMPTY_RPC).send();
    eventManager
        .invoke(address)
        .allocateCost(444)
        .withPayload(EventCreator.EMPTY_RPC)
        .sendFromContract();
    eventManager
        .deployContract(address)
        .allocateCost(444)
        .withPayload(EventCreator.EMPTY_RPC)
        .withContractJar(new byte[0])
        .withBinderJar(new byte[0])
        .sendFromContract();
    eventManager
        .deployContract(address)
        .allocateRemainingCost()
        .withPayload(EventCreator.EMPTY_RPC)
        .withContractJar(new byte[0])
        .withBinderJar(new byte[0])
        .send();
    eventManager
        .deployContract(address)
        .allocateCostFromContract(200)
        .withPayload(EventCreator.EMPTY_RPC)
        .withContractJar(new byte[0])
        .withBinderJar(new byte[0])
        .sendFromContract();
    eventManager
        .invoke(address)
        .allocateCostFromContract(200)
        .withPayload(EventCreator.EMPTY_RPC)
        .sendFromContract();
    eventManager
        .upgradeContract(address)
        .withNewBinderJar(new byte[0])
        .withNewContractJar(new byte[0])
        .withNewAbi(new byte[0])
        .withUpgradeRpc(EventCreator.EMPTY_RPC)
        .allocateCost(88)
        .send();
    eventManager
        .upgradeContract(address)
        .withNewBinderJar(new byte[0])
        .withNewContractJar(new byte[0])
        .withNewAbi(new byte[0])
        .withUpgradeRpc(EventCreator.EMPTY_RPC)
        .send();
    eventManager
        .upgradeContract(address)
        .withNewBinderJar(new byte[0])
        .withNewContractJar(new byte[0])
        .withNewAbi(new byte[0])
        .withUpgradeRpc(EventCreator.EMPTY_RPC)
        .sendFromContract();

    if (cost == null) {
      eventManager.registerCallbackWithCostFromRemaining(EventCreator.EMPTY_RPC);
    } else if (fromContract) {
      eventManager.registerCallbackWithCostFromContract(EventCreator.EMPTY_RPC, cost);
    } else {
      eventManager.registerCallback(EventCreator.EMPTY_RPC, cost);
    }

    SystemEventCreator eventCreator = sysContext.getInvocationCreator();
    eventCreator.invoke(address).withPayload(EventCreator.EMPTY_RPC).allocateCost(123).send();
    eventCreator.invoke(address).withPayload(EventCreator.EMPTY_RPC).allocateRemainingCost().send();
    eventCreator.setFeature("test", "test");
    eventCreator.invoke(address).withPayload(EventCreator.EMPTY_RPC).allocateCost(1234).send();

    eventCreator = sysContext.getInvocationCreator();
    eventCreator.invoke(address).allocateCost(0).withPayload(EventCreator.EMPTY_RPC).send();
    eventCreator.invoke(address).withPayload(EventCreator.EMPTY_RPC).allocateCost(37).send();

    sysContext.distributeGas();
    List<BinderEvent> events = sysContext.getInteractions();
    Assertions.assertThat(events).hasSize(6);

    CallResult remoteCalls = sysContext.getCallResult();
    Assertions.assertThat(remoteCalls).isInstanceOf(BinderEventGroup.class);
    BinderEventGroup<?> eventGroup = (BinderEventGroup<?>) remoteCalls;
    if (cost == null) {
      Assertions.assertThat(eventGroup.getCallbackCost()).isEqualTo(computedSplit);
    } else {
      Assertions.assertThat(eventGroup.getCallbackCost()).isEqualTo(cost);
    }
    Assertions.assertThat(eventGroup.getCallbackRpc()).isNotNull();

    BinderEvent binderEvent = eventGroup.getEvents().get(0);
    Assertions.assertThat(binderEvent).isInstanceOf(BinderInteraction.class);
    BinderInteraction interaction = (BinderInteraction) binderEvent;
    Assertions.assertThat(interaction.originalSender).isTrue();
    Assertions.assertThat(interaction.contract).isEqualTo(address);
    Assertions.assertThat(interaction.effectiveCost).isEqualTo(computedSplit);
    Assertions.assertThat(interaction.rpc).isEmpty();
    binderEvent = eventGroup.getEvents().get(1);
    interaction = (BinderInteraction) binderEvent;
    Assertions.assertThat(interaction.originalSender).isFalse();
    Assertions.assertThat(interaction.contract).isEqualTo(address);
    Assertions.assertThat(interaction.effectiveCost).isEqualTo(444);
    Assertions.assertThat(interaction.rpc).isEmpty();
    binderEvent = eventGroup.getEvents().get(2);
    BinderDeploy deploy = (BinderDeploy) binderEvent;
    Assertions.assertThat(deploy.originalSender).isFalse();
    Assertions.assertThat(deploy.contract).isEqualTo(address);
    Assertions.assertThat(deploy.effectiveCost).isEqualTo(444);
    Assertions.assertThat(deploy.rpc).isEmpty();
    binderEvent = eventGroup.getEvents().get(3);
    deploy = (BinderDeploy) binderEvent;
    Assertions.assertThat(deploy.originalSender).isTrue();
    Assertions.assertThat(deploy.contract).isEqualTo(address);
    Assertions.assertThat(deploy.effectiveCost).isEqualTo(computedSplit);
    Assertions.assertThat(deploy.rpc).isEmpty();
    binderEvent = events.get(0);
    interaction = (BinderInteraction) binderEvent;
    Assertions.assertThat(interaction.originalSender).isTrue();
    Assertions.assertThat(interaction.contract).isEqualTo(address);
    Assertions.assertThat(interaction.effectiveCost).isEqualTo(123);
    Assertions.assertThat(interaction.rpc).isEmpty();
    binderEvent = eventGroup.getEvents().get(6);
    Assertions.assertThat(binderEvent).isInstanceOf(BinderUpgrade.class);
    BinderUpgrade upgrade = (BinderUpgrade) binderEvent;
    Assertions.assertThat(upgrade.contract).isEqualTo(address);
    Assertions.assertThat(upgrade.allocatedCost).isEqualTo(88);
    binderEvent = eventGroup.getEvents().get(7);
    Assertions.assertThat(binderEvent).isInstanceOf(BinderUpgrade.class);
    upgrade = (BinderUpgrade) binderEvent;
    Assertions.assertThat(upgrade.contract).isEqualTo(address);
    Assertions.assertThat(upgrade.allocatedCost).isEqualTo(computedSplit);

    sysContext.setResult(EventCreator.EMPTY_RPC);
    Assertions.assertThatThrownBy(() -> sysContext.getCallResult())
        .isInstanceOf(IllegalStateException.class);
  }

  @Test
  void payServiceFees() {
    sysContext.deductFee(100);

    sysContext.payServiceFees(AVAILABLE_GAS - 100, address);
    Assertions.assertThatThrownBy(() -> sysContext.payServiceFees(1, address))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Unable to pay service fee from transaction. Needed 1, but only 0 available.");
  }

  @Test
  void setResult() {
    Assertions.assertThat(sysContext.getCallResult()).isNull();
    sysContext.setResult(EventCreator.EMPTY_RPC);
    CallResult remoteCalls = sysContext.getCallResult();
    Assertions.assertThat(remoteCalls).isInstanceOf(CallReturnValue.class);
    CallReturnValue calls = (CallReturnValue) remoteCalls;
    Assertions.assertThat(calls.getResult()).hasSize(0);
  }

  @Test
  public void eventsWithoutGas() {
    SystemEventCreator eventManager = sysContext.getInvocationCreator();
    eventManager
        .invoke(address)
        .withPayload(EventCreator.EMPTY_RPC)
        .allocateCost(AVAILABLE_GAS + 1)
        .send();

    Assertions.assertThatThrownBy(() -> sysContext.distributeGas())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot allocate gas for events: -1");
  }

  @Test
  public void eventsWithPreciseGas() {
    SystemEventCreator eventManager = sysContext.getInvocationCreator();
    eventManager
        .invoke(address)
        .withPayload(EventCreator.EMPTY_RPC)
        .allocateCost(AVAILABLE_GAS)
        .send();
    sysContext.distributeGas();
  }
}
